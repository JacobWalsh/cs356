"""
Basic routing table that responds to requests
Written by:
    Jacob Walsh
    UTEID: jdw3853
"""


from socket import *
from sys import *
import time 
import os.path
import argparse

import ipaddress
routing = {ipaddress.ip_network("0.0.0.0/0") : ("A", 100)}

def query(commandl):
    minCost = 100
    minRoute = "A"
    prefixLength = 0

    ipAddressQuery = ipaddress.ip_address(commandl[1])

    for i in routing.keys():
        if ipAddressQuery in i:
            potentialRoute = routing[i][0]
            potentialCost = routing[i][1]
            potentialPrefix = i.prefixlen

            if potentialCost < minCost:
                minCost = potentialCost
                minRoute = potentialRoute
                prefixLength = potentialPrefix 

            elif potentialCost == minCost:
                if potentialPrefix > prefixLength:
                    minCost = potentialCost
                    minRoute = potentialRoute
                    prefixLength = potentialPrefix 
    
    reply = "RESULT\r\n"
    reply += commandl[1] + " " + minRoute + " " + str(minCost) + "\r\n"
    reply += "END\r\n"

    return reply

def update(commandl):
    body = commandl[1:-2]
    for i in body: 
        entry =  i.split(" ")
        route = entry[0]
        subnet = ipaddress.ip_network(entry[1])
        cost = int(entry[2])
        


        # If the route is already included, only replace the origonal range if
        # the new range is a lower cost or if the costs being equal, a new route 
        # is added
        if subnet in routing:
            oldCost = routing[subnet][1] 
            oldRoute = routing[subnet][0] 

            if (oldCost > cost):
                routing[subnet] = (route, cost)
            if (oldCost == cost) and (oldRoute != route):
                routing[subnet] = (route, cost)
                continue

                # #there is no reason this subnet contains anything that needs to
                # be removed because if it did, it would have romoved it when added due to
                # # the fact the old and new share a prefix length and host id


    # If the new route encompases a previous IP subnet, add it to the list
    # if the older, more specific subnet is slower, remove it

    #if the new route is encompased by a previous subnet, only add it if it
    #provides a speed boost or is the same cost but provides a new route
        wasContainedorContains = False
        containmentAdd = False

        for k,v in routing.items():
            oldRoute = v[0]
            oldCost = v[1]
            kNetmask = ipaddress.ip_address(k.with_netmask.split("/")[1])
            subnetNetmask = ipaddress.ip_address(subnet.with_netmask.split("/")[1])

            keysToDelete = []

            #the new subnet is broader than the origional
            if subnetNetmask < kNetmask:
                wasContainedorContains = True
                containmentAdd = True

                #the old way was both narrower and slower, remove it
                if oldCost > cost:
                    keysToDelete.append(k)

                #the old way was the same cost and route but narrower making it redundant
                if (oldCost == cost) and (route == oldRoute):
                    keysToDelete.append(k)

            #the new subnet is narrower than the origional
            elif subnetNetmask > kNetmask:
                wasContainedorContains = True

                #add only if we speed up over our narrower range or if the
                #route is different without sacrificing cost

                if oldCost > cost:
                    containmentAdd = True

                if (oldCost == cost) and (oldRoute != route):
                    containmentAdd = True


        #avoid modifying dict being iterated over
        for deadItem in keysToDelete:
            del routing[deadItem]

        # we know we were not contained, we do not contain anyone,
        # and we do not match any one
        # If the route does not include any existing IP, add it
        if not wasContainedorContains or containmentAdd:
            routing[subnet] = (route, cost)

    result = "ACK\r\n"
    result += "END\r\n"
    return result

def parse_command(command):
    lines = command.split('\r\n')
    result = ""
    if len(lines) < 3:
        raise Exception("Inproperly formed query")
    if lines[0] == "UPDATE":
        result = update(lines)
    elif lines[0] == "QUERY":
        result = query(lines)
    return result

def start_server(port):
    server_name = 'localhost'
    # print port
    serverSocket =  socket(AF_INET,SOCK_STREAM)
    serverSocket.bind(('',port))
    serverSocket.listen(1)
    print('The server is ready to receive on port: ' + str(port))
    print("------------------------------------")

    
    while 1:
        connectionSocket,addr = serverSocket.accept()
        sentence = connectionSocket.recv(2048)
        result = parse_command(sentence.decode())
        connectionSocket.send(result.encode()) 
        connectionSocket.close()
        print("------------------------------------")



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('port', type=int, help='the port number to run the server off of')
    args = parser.parse_args()
    start_server(args.port)



if __name__ == '__main__':
    main()




