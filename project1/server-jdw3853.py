"""
Basic HTTP web server that only supports GET
Written by:
    Jacob Walsh
    UTEID: jdw3853
"""


from socket import *
from sys import *
import time 
import os.path
import argparse



supported_file_types = {".html" : "text/html", ".htm" : "text/html", ".txt" : "text/plain", ".jpg" : "image/jpeg", ".jpeg" : "image/jpeg"}
HTTP_code = {"200" : "OK", "304" : "Not Modified", "400" : "Bad Request", "404" : "Not Found", "415" : "Unsupported Media Type", "505" : "HTTP Version Not Supported"}

#in the message, use the HTTP Error code that we want to return
class HTTPException(Exception):
    def __init__(self,msg):
        Exception.__init__(self, msg)

# Errors to handle:
    # unsuported media type (ie, not .html, .txt and .jpeg) *
    # Method other than GET *
    # Wrong HTTP version *
    # missing required header *
    # cr/lf wrong or missing *
    #file exists, * 
    #if conditional get, find out if we satisfy the needed date in order to send back the file *

def get_time(timeString):
    date = None
    try:
        date = time.strptime(timeString, '%a, %d %b %Y %H:%M:%S %Z')
    except ValueError:
        try:
            date = time.strptime(timeString)
        except ValueError:
            try:
                date = time.strptime(timeString, '%A, %d-%b-%y %H:%M:%S %Z')
            except ValueError:
                return None
    print(timeString)
    return date

def validate_request(request_http):
    print(request_http)

    lines = request_http.split('\r\n')

    #the required 2 lines dont exist
    if len(lines) < 2:
        raise HTTPException("400")
    
    #first line misformed
    first_line = lines[0].split(" ")
    if len(first_line) != 3:
        raise HTTPException("400")

    if first_line[0] != "GET":
        raise HTTPException("400")

    http_dict = {}

    http_dict["file_to_get"] = first_line[1]

    #file type not supported
    file_type_index = first_line[1].rfind(".")
    if file_type_index == None:
        raise HTTPException("415")

    #file type not supported
    http_dict["file_type"] = first_line[1][file_type_index:]
    if http_dict["file_type"] not in supported_file_types:
        raise HTTPException("415")

    #file doesnt exist
    if not os.path.isfile("." + http_dict["file_to_get"]):
        raise HTTPException("404")

    #wrong HTTP version
    if first_line[2] != "HTTP/1.1":
        raise HTTPException("505")
    
    #make sure we have the valid required second line
    if lines[1].split(" ")[0] != "Host:":
        raise HTTPException("400")



    #form a dict with all the values in the HTTP request
    if lines[-2] != "" or lines[-1] != "":
        raise HTTPException("400")

    for i,v in enumerate(lines[1:-2]):
        colon_index = v.find(":")
        header_type = v[:colon_index]
        header_value = v[colon_index+1 :]


        #exceptions are for compatibility with browsers, may break with other headers not listed whose values contain spaces
        if len(v.split(" ")) != 2 and header_type != "If-Modified-Since" and header_type != "Date" and header_type != "Last-Modified" and header_type != "User-Agent" and header_type != "Accept-Encoding":  
            raise HTTPException("400")

        http_dict[header_type] = header_value
        


    file_time = time.gmtime(os.path.getmtime("." + http_dict["file_to_get"]))
    http_dict["server_last_modified"] = time.strftime("%a, %d %b %Y %H:%M:%S %Z", file_time)

    if "If-Modified-Since" in http_dict:
        if file_time <= get_time(http_dict["If-Modified-Since"][1:]):
            raise HTTPException("304")

    

    return http_dict
    

def craft_response(input_tuple):

    code, http_dict = input_tuple

    response = "Connection: close\r\n"
    response += "Server: Phill\r\n"

    tnow = time.gmtime()
    tnowstr = time.strftime("%a, %d %b %Y %H:%M:%S %Z", tnow)
    response += "Date: " + tnowstr + "\r\n"
    data = None

    if code != "200":
        #here we will have the raw http
        response = "HTTP/1.1 " + code + " " + HTTP_code[code] + "\r\n" + response
        response += "\r\n"

        print ("HTTP Error: " + code)
    else:
        #here we will have the http dict for use
        response = "HTTP/1.1 200 OK\r\n" + response

        response += "Last-Modified: " + http_dict["server_last_modified"] + " \r\n" 

        data,length = get_data(http_dict)

        response += "Content-Length: " + str(length) + "\r\n"

        response += "Content-Type: " + supported_file_types[http_dict["file_type"]] + "\r\n"

        response += "\r\n"
        

        print ("Good request")
    return response, data

def get_data(http_dict):
    result = ""
    if supported_file_types[http_dict["file_type"]] == "image/jpeg":
        with open("."+ http_dict["file_to_get"], "rb") as image:
            result = image.read()
    else:
        with open("."+ http_dict["file_to_get"], "r") as text:
            result = text.read().encode()
    return result, len(result)



def start_server(port):
    server_name = 'localhost'
    # print port
    serverSocket =  socket(AF_INET,SOCK_STREAM)
    serverSocket.bind(('',port))
    serverSocket.listen(1)
    print('The server is ready to receive on port: ' + str(port))

    print("------------------------------------")
    while 1:
        connectionSocket,addr = serverSocket.accept()
        sentence = connectionSocket.recv(2048)
        http_object = sentence.decode()

        if len(http_object) == 0:
            connectionSocket.close()
            continue

        response_prep = () #default is all is a-okay
        try:
           response_prep = ("200", validate_request(http_object))
        except HTTPException as e: 
            response_prep = (str(e), http_object)
        finally:
            text,data = craft_response(response_prep)
            connectionSocket.send(text.encode())
            if data != None:
                connectionSocket.send(data)

            connectionSocket.close()
            print("------------------------------------")



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('port', type=int, help='the port number to run the server off of')
    args = parser.parse_args()
    start_server(args.port)



if __name__ == '__main__':
    main()



